

WORDLIST_FILENAME = "words.txt"

def loadWords(file_name):
    """
    Returns a dictionary of valid words. Words are strings of lowercase letters.

    Depending on the size of the word list, this function may
    take a while to finish.
    """
    print "Loading word list from file into a dictionary..."
    # inFile: file
    inFile = open(file_name, 'r', 0)
    # wordList: list of strings
    wordList = {}
    cnt = 0
    for line in inFile:
        wordList[cnt] = line.strip().lower()
        cnt += 1
    print "  ", len(wordList), "words loaded."
    return wordList

print loadWords(WORDLIST_FILENAME).keys()

print 'done'
