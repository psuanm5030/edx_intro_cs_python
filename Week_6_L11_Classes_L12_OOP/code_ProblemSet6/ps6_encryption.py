# 6.00x Problem Set 6
#
# Part 1 - HAIL CAESAR!

import string
import random

WORDLIST_FILENAME = "words.txt"

# -----------------------------------
# Helper code
# (you don't need to understand this helper code)
def loadWords():
    """
    Returns a list of valid words. Words are strings of lowercase letters.
    
    Depending on the size of the word list, this function may
    take a while to finish.
    """
    print "Loading word list from file..."
    inFile = open(WORDLIST_FILENAME, 'r')
    wordList = inFile.read().split()
    print "  ", len(wordList), "words loaded."
    return wordList

def isWord(wordList, word):
    """
    Determines if word is a valid word.

    wordList: list of words in the dictionary.
    word: a possible word.
    returns True if word is in wordList.

    Example:
    ### >>> isWord(wordList, 'bat') returns
    True
    ### >>> isWord(wordList, 'asdf') returns
    False
    """
    word = word.lower()
    word = word.strip(" !@#$%^&*()-_+={}[]|\\:;'<>?,./\"")
    return word in wordList

def randomWord(wordList):
    """
    Returns a random word.

    wordList: list of words  
    returns: a word from wordList at random
    """
    return random.choice(wordList)

def randomString(wordList, n):
    """
    Returns a string containing n random words from wordList

    wordList: list of words
    returns: a string of random words separated by spaces.
    """
    return " ".join([randomWord(wordList) for _ in range(n)])

def randomScrambled(wordList, n):
    """
    Generates a test string by generating an n-word random string
    and encrypting it with a sequence of random shifts.

    wordList: list of words
    n: number of random words to generate and scamble
    returns: a scrambled string of n random words

    NOTE:
    This function will ONLY work once you have completed your
    implementation of applyShifts!
    """
    s = randomString(wordList, n) + " "
    shifts = [(i, random.randint(0, 25)) for i in range(len(s)) if s[i-1] == ' ']
    return applyShifts(s, shifts)[:-1]

def getStoryString():
    """
    Returns a story in encrypted text.
    """
    return open("story.txt", "r").read().rstrip()


# (end of helper code)
# -----------------------------------

#
# Problem 1: Encryption
#
def buildCoder(shift):
    """
    Returns a dict that can apply a Caesar cipher to a letter.
    The cipher is defined by the shift value. Ignores non-letter characters
    like punctuation, numbers and spaces.

    shift: 0 <= int < 26
    returns: dict
    """
    letters_lower, letters_upper = string.ascii_lowercase, string.ascii_uppercase
    lower_dict, upper_dict = {}, {}
    cnt = 0
    for val in range(len(letters_lower)):
        if val < (len(letters_lower) - shift):
            lower_dict[letters_lower[val]] = letters_lower[val+shift]
        else:
            lower_dict[letters_lower[val]] = letters_lower[0+cnt]
            cnt += 1
    cnt = 0
    for val in range(len(letters_upper)):
        if val < (len(letters_upper) - shift):
            upper_dict[letters_upper[val]] = letters_upper[val+shift]
        else:
            upper_dict[letters_upper[val]] = letters_upper[0+cnt]
            cnt += 1
    #letters_dict = dict(lower_dict.items() + upper_dict.items()) # slower method of combining dictionaries
    letters_dict = dict(lower_dict,**upper_dict)
    #letters_dict = dict((letters[val],val) for val in range(len(letters)))
    return letters_dict

#print buildCoder(9)

def applyCoder(text, coder):
    """
    Applies the coder to the text. Returns the encoded text.

    text: string
    coder: dict with mappings of characters to shifted characters
    returns: text after mapping coder chars to original text
    """
    p = string.punctuation + ' ' + string.digits
    result = ''
    for char in text:
        #print 'char is --> ', char
        if char in p:
            result += char
        else:
            result += coder[char]
        #print 'result is --> ',result
    return result

#print applyCoder('Khoor, zruog!',buildCoder(23))

def applyShift(text, shift):
    """
    Given a text, returns a new text Caesar shifted by the given shift
    offset. Lower case letters should remain lower case, upper case
    letters should remain upper case, and all other punctuation should
    stay as it is.

    text: string to apply the shift to
    shift: amount to shift the text (0 <= int < 26)
    returns: text after being shifted by specified amount.
    """
    return applyCoder(text,buildCoder(shift))

#
# Problem 2: Decryption
#
def findBestShift(wordList, text):
    """
    Finds a shift key that can decrypt the encoded text.

    text: string
    returns: 0 <= int < 26
    """
    # Need to setup a dictionary (shift, number of words) to store the best shift key per the algorithm below
    shift_dict = {}
    # Split the text into a list of words.
    l_text = text.split(' ')
    #print l_text
    # Setup a for loop to loop from 1 to 26, testing all the shift keys possible
    for x in range(1,26):
        cnt = 0
        # Then loop, testing each word using isWord().  If True, add 1 to counter
        for word in l_text:
        # Then convert the word using the applyShift() helper.
            word_shift = applyShift(word,x)
            if isWord(wordList,word_shift):
                cnt += 1
        # Outside of loop, check dictionary for highest value and return that key.
        shift_dict[x] = shift_dict.get(x,0) + cnt
    # print shift_dict
    # find the max count of keys and return it.  IF the max_value is 0, then return 0 as best shift
    # print  max(shift_dict.values(), key=int)
    max_value = max(shift_dict.values(), key=int)
    if max_value == 0: return max_value
    for k,v in shift_dict.items():
        if v == max_value:
            max_key = k
    return max_key

def decryptStory():
    """
    Using the methods you created in this problem set,
    decrypt the story given by the function getStoryString().
    Use the functions getStoryString and loadWords to get the
    raw data you need.

    returns: string - story in plain text
    """
    wordlist = loadWords()
    story = getStoryString()
    best_shift = findBestShift(wordlist, story)
    return applyShift(story,best_shift)


#
# Build data structures used for entire session and run encryption
#

if __name__ == '__main__':
    # To test findBestShift:
    # wordList = loadWords()
    # s = applyShift('Hello, world!', 8)
    # s1 = applyShift('struggle neeD sick exPlore reason',0)
    # #bestShift = findBestShift(wordList, s)
    # bestShift = findBestShift(wordList, s1)
    # print findBestShift(wordList, s1)
    # print applyShift(s1,22)
    #print applyShift(s,18)
    #assert applyShift(s, bestShift) == 'Hello, world!'
    # To test decryptStory, comment the above four lines and uncomment this line:
    print decryptStory()
