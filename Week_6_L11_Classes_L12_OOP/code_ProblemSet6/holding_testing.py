__author__ = 'Miller'
letters_lower, letters_upper = string.ascii_lowercase, string.ascii_uppercase
lower_dict, upper_dict = {}, {}
for val in range(len(letters_lower)):
    if val < 23:
        lower_dict[letters_lower[val]] = letters_lower[val+3]
    elif val == 23:
        lower_dict[letters_lower[val]] = letters_lower[0]
    elif val == 24:
        lower_dict[letters_lower[val]] = letters_lower[1]
    elif val == 25:
        lower_dict[letters_lower[val]] = letters_lower[2]
for val in range(len(letters_upper)):
    if val < 23:
        upper_dict[letters_upper[val]] = letters_upper[val+3]
    elif val == 23:
        upper_dict[letters_upper[val]] = letters_upper[0]
    elif val == 24:
        upper_dict[letters_upper[val]] = letters_upper[1]
    elif val == 25:
        upper_dict[letters_upper[val]] = letters_upper[2]
#letters_dict = dict(lower_dict.items() + upper_dict.items()) # slower method of combining dictionaries
letters_dict = dict(lower_dict,**upper_dict)
#letters_dict = dict((letters[val],val) for val in range(len(letters)))
return letters_dict