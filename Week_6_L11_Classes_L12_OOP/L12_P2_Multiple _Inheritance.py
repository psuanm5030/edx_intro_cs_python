__author__ = 'Miller'

class A(object):
    def __init__(self):
        self.a = 1
    def x(self):
        print "A.x"
    def y(self):
        print "A.y"
    def z(self):
        print "A.z"

class B(A):
    def __init__(self):
        A.__init__(self)
        self.a = 2
        self.b = 3
    def y(self):
        print "B.y"
    def z(self):
        print "B.z"

class C(object):
    def __init__(self):
        self.a = 4
        self.c = 5
    def y(self):
        print "C.y"
    def z(self):
        print "C.z"

class D(C, B):
    def __init__(self):
        C.__init__(self)
        B.__init__(self)
        self.d = 6
    def z(self):
        print "D.z"

print obj.a # Prints 2 because the second __init__ overrides the first
print obj.b # prints 3 because its not present in first __init__ or self, but its present in B
print obj.c # prints 5 because its present in first __init__ but NOT in second (no override)
print obj.d # prints 6 because its present in self (if was present in others, it would be overriden)
obj.x() # prints A.x because class D inherits from B which inherits from A (only present there)
obj.y() # prints C.y because its present in class C (others are ignored)
obj.z() # prints D.z becasue its present in D and doesnt need to search others.