# Implemented a class and extra methods to interset two lists and to define the length

class intSet(object):
    """An intSet is a set of integers
    The value is represented by a list of ints, self.vals.
    Each int in the set occurs in self.vals exactly once."""

    def __init__(self):
        """Create an empty set of integers"""
        self.vals = []

    def insert(self, e):
        """Assumes e is an integer and inserts e into self"""
        if not e in self.vals:
            self.vals.append(e)

    def member(self, e):
        """Assumes e is an integer
           Returns True if e is in self, and False otherwise"""
        return e in self.vals

    def remove(self, e):
        """Assumes e is an integer and removes e from self
           Raises ValueError if e is not in self"""
        try:
            self.vals.remove(e)
        except:
            raise ValueError(str(e) + ' not found')


    def intersect(self, other):
        """Returns a new object that contains both elements in common"""
        # Initialize a new intSet
        commonValueSet = intSet()
        # Go through the values in this set
        for elt in self.vals:
            # Check if each value is a member of the other set
            if other.member(elt):
                commonValueSet.insert(elt)
        return commonValueSet

    def __len__(self):
        """Returns the length of the set.
           This method is called by the `len` built-in function."""
        return len(self.vals)

    def __str__(self):
        """Returns a string representation of self"""
        self.vals.sort()
        return '{' + ','.join([str(e) for e in self.vals]) + '}'

s1 = intSet()
s2 = intSet()
s1.insert(1)
s1.insert(2)
s1.insert(3)
s2.insert(3)
s2.insert(4)
s2.insert(5)
print s1.intersect(s2)
print len(s1)