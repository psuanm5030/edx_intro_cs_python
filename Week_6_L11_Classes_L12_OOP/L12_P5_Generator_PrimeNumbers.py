__author__ = 'Miller'

def genPrimes():
    prime_list = []
    pot = 2
    prime_list.append(pot)
    yield prime_list[-1]
    pot += 1
    while True:
        trig = True
        for n in prime_list:
            if pot % n == 0:
                trig = False
                break
        if trig == True:
            prime_list.append(pot)
            yield prime_list[-1]
        pot += 1