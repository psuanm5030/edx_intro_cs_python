__author__ = 'Miller'


class Queue(object):
    def __init__(self):
        self.line = []

    def insert(self,i_elt):
        """Inserts one element into the line"""
        self.line.append(i_elt)

    def remove(self):
        """Removes or pops one element from the line and returns it.  If the queue is empty, raise a ValueError"""
        try:
            return self.line.pop(0)
            #return rem
        except:
            raise ValueError()

queue = Queue()
queue.insert(5)
queue.insert(6)
queue.remove() # should print 5
queue.insert(7)
queue.remove() # should print 6
queue.remove() # should print 7
queue.remove() # should raise a ValueError