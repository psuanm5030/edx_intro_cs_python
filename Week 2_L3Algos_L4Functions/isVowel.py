def isvowel(char):
    '''
    char: a single letter of any case

    returns: True if char is a vowel and False otherwise.
    '''
    char = char.lower()
    print char
    if char == 'a' or char == 'e' or char =='i' or char == 'o' or char == 'u': 
        return True
    else:
        return False
        
print isvowel('a')
print isvowel('A')