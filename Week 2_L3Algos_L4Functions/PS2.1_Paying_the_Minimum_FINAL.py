#PS2.1 - Paying off the credit card
# Problem 1: Paying the minimum.  The point is to use fixed monthly payment to calculate the balance in a year,
# including the total paid.

#Do not paste these variables into the class coder.
balance = 4213.00
annualInterestRate = 0.20
monthlyPaymentRate = 0.04

monthly_int_rate = annualInterestRate / 12.0
min_monthly = 0.00
rem_balance = balance
total_paid = 0.00

for month in range(1,13): 
    #calculate the minimum payment, then subtract from the balance
    min_monthly = round(rem_balance * monthlyPaymentRate,2)
    rem_balance = round(rem_balance - min_monthly,2)
    
    #calculate the interest accrued on rem_balance, then add to rem_balance
    interest_accrued = round(rem_balance * monthly_int_rate,2)
    #print 'INTEREST: ',interest_accrued
    rem_balance = rem_balance + interest_accrued
    
    #calculate total_paid by adding the min_monthly every month
    total_paid += min_monthly
    
    print 'Month: ',month
    print 'Minimum monthly payment: ',min_monthly
    print 'Remaining balance: ',rem_balance
    if month == 12: 
        print 'Total paid: ',total_paid
        print 'Remaining balance: ',rem_balance