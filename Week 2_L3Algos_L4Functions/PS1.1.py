

s = 'azcbobobegghakl'

dict={}
for letter in s:
    dict[letter]= dict.get(letter,0)+1
    #NOTE - not using the below loop as the syntax above is more convenient.
    #if letter not in dict:
    #    dict[letter]=1
    #else:
    #    dict[letter] += 1

value = 0
for vowel in "aeiou":
    try:
        value = value + dict[vowel]
    except:
        continue

#dict["a"]+dict["e"]+dict["i"]+dict["o"]+dict["u"]

print "Number of Vowels: ",value
