#Bisection search of the square root

x = 25 #this is the number we are trying to find the sq. root of
low = 0.0 #if this isnt float, you lose precision
high = x
epsilon = 0.01 #this is the precision value and step value
ans = (high + low)/2
numguesses = 0 
while abs(ans**2 - x) >= epsilon: 
    print 'Low: ',low,'High: ',high, 'Answer: ', ans
    numguesses += 1
    if ans**2 < x:
        low = ans
    else: 
        high = ans
    ans = (low + high)/2
    
print 'Guesses: ',numguesses
print 'The sq root of '+str(x)+' is '+str(ans)+'.'
    
    