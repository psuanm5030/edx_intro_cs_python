def clip(lo, x, hi):
    '''
    Takes in three numbers and returns a value based on the value of x.
    Returns:
     - lo, when x < lo
     - hi, when x > hi
     - x, otherwise
     assume that lo < hi
    '''
    #result = max(min(lo,x), min(hi,x))
    return min(max(lo,x),hi)

lo = 1
x = -1
hi = 3
result = clip(lo, x, hi)
print result