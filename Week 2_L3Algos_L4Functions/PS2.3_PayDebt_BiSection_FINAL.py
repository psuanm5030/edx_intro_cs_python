#PS2.3 - Paying debt off in a year - USING BISECTION SEARCH
# Problem 3: Calculate the smallest monthly payment to pay off balance within one year

#Do not paste these variables into the class coder.
balance = 5000
annualInterestRate = 0.20

#set the epsilon value, which is the precision target.  You want to be within ONE CENT
epsilon = 0.01
#Lower bound should be the minimum, so the least you would have to pay monthly would be the balance /12 (no interest)
#Upper bound should be the maximum, so the most you would have to pay.  That is the 1/12 of balance plus all the interest compounded.
lower_bound = balance / 12
upper_bound = (balance * ((1+annualInterestRate)**12)/12)
monthly_int_rate = annualInterestRate / 12.0

#Set the first fixed_monthly guess
fixed_monthly = (upper_bound + lower_bound)/2
num_guesses = 0
rem_balance = balance

#Run the loop searching for the answer until it is within ONE CENT
while not(rem_balance >= -epsilon and rem_balance <= epsilon): 
    #print 'Lower: ',lower_bound,'Upper: ',upper_bound,' Fixed payment: ',fixed_monthly, 'Remaining Balance: ',rem_balance
    num_guesses += 1
    #Each time you run the while loop, you need to start with the balance as proscribed.
    rem_balance = balance
    
    #Run for 12 months using the fixed monthly you calculated (first run: above; other runs: calculated after for loop)
    for month in range(1,13):
        #calculate the remaining balance
        rem_balance = rem_balance - fixed_monthly
        
        #calculate the interest accrued on rem_balance, then add to rem_balance
        interest_accrued = rem_balance * monthly_int_rate
        rem_balance = rem_balance + interest_accrued
    
    #Calculating new boundaries.
    #If the remaining balance is less than 0, then our fixed monthly is too high (we paid off TOO much)
    #THUS set new upper_bound to the fixed_amount
    if rem_balance < -0.01:
        upper_bound = fixed_monthly
    #If the remaining balance is greater than 0, then our fixed monthly is too low (we didnt pay off ENOUGH)
    #THUS set the new lower_bound to the fixed_amount
    elif rem_balance > 0.01: 
        lower_bound = fixed_monthly
    
    #Calculate a new fixed_monthly guess
    fixed_monthly = (upper_bound + lower_bound)/2

print 'Lowest Payment: ',round(fixed_monthly,2)
#print 'Guesses: ', num_guesses
