epsilon = 0.01
y = 24.0
guess = y/2.0 #initial guess - not very good, but a start

while abs(guess*guess - y) >= epsilon: #do this next part until within 0.01
    guess = guess - (((guess**2) - y)/(2*guess)) #newton-raphson formula
    print guess
print('Sq root of' + str(y) + 'is about ' + str(guess))