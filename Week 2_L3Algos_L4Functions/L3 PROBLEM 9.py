#L3 PROBLEM 9 - finding the secret number using bi-section search

high = 100
low = 0

print('Please think of a number between 0 and 100!')
guess = (low + high)/2
resp='h'
while resp != 'c':
    print('Is your secret number '),
    print(guess),
    print('!')
    resp = raw_input("Enter 'h' to indicate the guess is too high. Enter 'l' to indicate the guess is too low. Enter 'c' to indicate I guessed correctly. ")
    
    if resp == 'l': 
        low = guess
    elif resp == 'h':
        high = guess
    elif resp == 'c':
        pass
    else:
        print('Sorry, I did not understand your input.')
        
    guess = (low+high)/2
    
print('Game over. Your secret number was:'),
print(guess)