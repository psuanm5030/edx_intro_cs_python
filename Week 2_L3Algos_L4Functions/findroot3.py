def findroot3(x, power, epsilon):
    """x and epsilon int or float, power an int
    epsilon > 0 & power >=1
    returns a float y s t. y**power is within epsilon of x.
    If such a float does not exist, it returns None."""
    if x < 0 and power%2 == 0:
        return none
    # cant find even powerered root of negative number
    low = min(-1,x)
    high = max(1,x)
    ans = (high+low)/2.0
    while abs(ans**power-x)>epsilon:
        if ans**power<x:
            low=ans
        else: 
            high = ans
        ans = (high+low)/2.0
    return ans