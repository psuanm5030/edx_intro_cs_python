#idea: iterate through, seeing if 'bob' is in the string beginning from
#the atpos (the position of the iterator.  If so, add one to count.

s = 'oboobobboobboobsabobb'

cnt = 0
for pos in range(len(s)):
    if s[pos] == 'b':
        if 'bob' in s[pos:pos+3]:
            cnt += 1 
        
print "Number of times bob occurs is: ",cnt
