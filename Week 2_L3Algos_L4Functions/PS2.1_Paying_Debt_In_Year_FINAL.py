#PS2.2 - Paying debt off in a year
# Problem 2: Calculate the FIXED amount needed to be paid monthly in order to come to zero balance.

#Do not paste these variables into the class coder.
balance = 5000
annualInterestRate = 0.20

epsilon = 0.01
step = 10
monthly_int_rate = annualInterestRate / 12.0

fixed_monthly = 0
num_guesses = 0
rem_balance = balance

while rem_balance > 0: 
    fixed_monthly += 0.01
    num_guesses+=1
    print 'Guesses: ',num_guesses,'Fixed Amt: ',fixed_monthly, 'Remaining Balance: ',rem_balance
    rem_balance = balance
    for month in range(1,13):
        #calculate the remaining balance
        rem_balance = round(rem_balance - fixed_monthly,2)
        
        #calculate the interest accrued on rem_balance, then add to rem_balance
        interest_accrued = round(rem_balance * monthly_int_rate,2)
        rem_balance = rem_balance + interest_accrued
    
print 'Remaining Balance: ',rem_balance,' Lowest Payment: ',fixed_monthly
