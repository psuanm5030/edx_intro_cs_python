__author__ = 'Miller'

def myLog(x, b):
    '''
    x: a positive integer - i.e. >= 1
    b: a positive integer; b >= 2

    returns: log_b(x), or, the logarithm of x relative to a base b.
    '''
    # Your Code Here

    if x < b: return 0
    if x == b: return 1
    else:
        return 1 + myLog(x/b,b)

print 'Answer: ',myLog(16,16) #answer - 1
print 'Answer: ',myLog(27,3) #answer - 3
print 'Answer: ',myLog(4,16) #answer - 0
print 'Answer: ',myLog(124,2) #answer - 6