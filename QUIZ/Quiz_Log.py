__author__ = 'Miller'

def myLog(x, b):
    '''
    x: a positive integer - i.e. >= 1
    b: a positive integer; b >= 2

    returns: log_b(x), or, the logarithm of x relative to a base b.
    '''
    # Your Code Here
    y = 1
    if x < b: return 0
    while b ** y < x and b ** (y+1) <= x:
        y = y+1
    return y

print 'Answer: ',myLog(16,16)