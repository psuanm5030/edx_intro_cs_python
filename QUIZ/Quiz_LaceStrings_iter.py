def laceStrings(s1, s2):
    """
    s1 and s2 are strings.

    Returns a new str with elements of s1 and s2 interlaced,
    beginning with s1. If strings are not of same length, 
    then the extra elements should appear at the end.
    """
    result = ''
    # If either string is blank -> return the other
    if len(s1) == 0:
        return s2
    elif len(s2) == 0:
        return s1
    elif len(s1) == 0 and len(s2) ==0:
        return ''
    # Find the smallest string and save in variable
    if len(s1) <= len(s2):
        smallest = s1
    else:
        smallest = s2
    # Loop from 0 to length of smallest, each time adding to the result
    for i in range(len(smallest)):
        result += s1[i] + s2[i]
    # If s2 is smallest, add the additional chars from s1,
    # else s1 is smallest, add addtl chars from s2
    #print 'CURRENT RESULT AFTER LOOP:'
    if s2 == smallest:
        #print 'in s2 smallest loop - len of s2', len(s2), ' len of s1', len(s1)
        result += s1[len(smallest):]
    else:
        result += s2[len(smallest):]
    return result


print 'final string: ',laceStrings('1357','2468') # 12345678
print 'final string: ',laceStrings('1357','246')  # 1234567
print 'final string: ',laceStrings('13','2468')  # 123468
print 'final string: ',laceStrings('','2468')    # 2468
print 'final string: ',laceStrings('13','')
print 'final string: ',laceStrings('','')