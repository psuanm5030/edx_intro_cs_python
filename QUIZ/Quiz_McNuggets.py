def McNuggets(n):
    """
    n is an int

    Returns True if some integer combination of 6, 9 and 20 equals n
    Otherwise returns False.
    """
    if n < 6: return False
    if n % 3 == 0 or n % 20 == 0: return True

    max = n / 20
    while max > 0:
        check = n - (max*20)
        if check % 3 == 0: return True
        max -= 1

    return False

print McNuggets(6)  #Answer --> True
print McNuggets(15) #Answer --> True
print McNuggets(16) #Answer --> False
print McNuggets(26) #Answer --> True
print McNuggets(27) #Answer --> True
print McNuggets(121) #Answer --> True