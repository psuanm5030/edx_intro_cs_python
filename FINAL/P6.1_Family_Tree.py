__author__ = 'Miller'
class Member(object): # Represents a single person in the family
    def __init__(self, founder):
        """
        founder: string
        Initializes a member.
        Name is the string of name of this node,
        parent is None, and no children
        """
        self.name = founder
        self.parent = None
        self.children = []

    def __str__(self):
        return self.name

    def add_parent(self, mother):
        """
        mother: Member
        Sets the parent of this node to the `mother` Member node
        """
        self.parent = mother

    def get_parent(self):
        """
        Returns the parent Member node of this Member
        """
        return self.parent

    def is_parent(self, mother):
        """
        mother: Member
        Returns: Boolean, whether or not `mother` is the
        parent of this Member
        """
        return self.parent == mother

    def add_child(self, child):
        """
        child: Member
        Adds another child Member node to this Member
        """
        self.children.append(child)

    def is_child(self, child):
        """
        child: Member
        Returns: Boolean, whether or not `child` is a
        child of this Member
        """
        return child in self.children


class Family(object): # Represents the whole family tree
    def __init__(self, founder):
        """
        Initialize with string of name of oldest ancestor

        Keyword arguments:
        founder -- string of name of oldest ancestor
        """

        self.names_to_nodes = {}
        self.root = Member(founder) #Creating a member class with the variable passed as the founder
        self.names_to_nodes[founder] = self.root #Create a dictionary with variable (name) and the founder member obj.

    def set_children(self, mother, list_of_children):
        """
        Set all children of the mother.

        Keyword arguments:
        mother -- mother's name as a string
        list_of_children -- children names as strings
        """
        # convert name to Member node (should check for validity)
        mom_node = self.names_to_nodes[mother]
        # add each child
        for c in list_of_children:
            # create Member node for a child
            c_member = Member(c)
            # remember its name to node mapping
            self.names_to_nodes[c] = c_member
            # set child's parent
            c_member.add_parent(mom_node)
            # set the parent's child
            mom_node.add_child(c_member)

    def is_parent(self, mother, kid):
        """
        Returns True or False whether mother is parent of kid.

        Keyword arguments:
        mother -- string of mother's name
        kid -- string of kid's name
        """
        mom_node = self.names_to_nodes[mother]
        child_node = self.names_to_nodes[kid]
        return child_node.is_parent(mom_node)

    def is_child(self, kid, mother):
        """
        Returns True or False whether kid is child of mother.

        Keyword arguments:
        kid -- string of kid's name
        mother -- string of mother's name
        """
        mom_node = self.names_to_nodes[mother]
        child_node = self.names_to_nodes[kid]
        return mom_node.is_child(child_node)

    def cousin(self, a, b):
        """
        Returns a tuple of (the cousin type, degree removed)

        Keyword arguments:
        a -- string that is the name of node a
        b -- string that is the name of node b

        cousin type:
          -1 if a and b are the same node.
          -1 if either one is a direct descendant of the other
          >=0 otherwise, it calculates the distance from
          each node to the common ancestor.  Then cousin type is
          set to the smaller of the two distances, as described
          in the exercises above

        degrees removed:
          >= 0
          The absolute value of the difference between the
          distance from each node to their common ancestor.
        """
        #print self.names_to_nodes
        c_type = None
        degree = None
        ancestors_A = [self.names_to_nodes[a].get_parent()]
        ancestors_B = [self.names_to_nodes[b].get_parent()]
        dist_a = 0
        dist_b = 0
        flag_a = False
        flag_b = False
        #Cousin type
        # if a and b are the same node
        #print self.names_to_nodes
        if self.names_to_nodes[a] == self.names_to_nodes[b]:
            c_type = -1
            degree = 0
        # if either one is a direct descendant of the other
        # elif self.is_child(a,b) or self.is_child(b,a):
        #     c_type = -1
        #     degree = 1 #just added
        elif self.names_to_nodes[b] in self.names_to_nodes and self.names_to_nodes[b] in self.names_to_nodes:
            c_type = -1
            degree = -1 #just added
        else:
            # Run while loops to retrieve all the parents for each node, add to list.
            trig = True
            while trig:
                if ancestors_A[-1] == None:
                    trig = False
                    flag_a = True
                elif ancestors_A[-1].get_parent() == None:
                    trig = False
                else:
                    ancestors_A.append(ancestors_A[-1].get_parent()) # This gets the parent
                    if ancestors_A[-1] == self.root:
                        trig = False
            trig = True
            while trig:
                if ancestors_B[-1] == None:
                    trig = False
                    flag_b = True
                elif ancestors_B[-1].get_parent() == None:
                    trig = False
                else:
                    ancestors_B.append(ancestors_B[-1].get_parent()) # This gets the parent
                    if ancestors_B[-1] == self.root:
                        trig = False
            # print 'anc A: ', ancestors_A
            # print 'anc B: ', ancestors_B
            # Find the common node in each of the lists of ancestors
            if ancestors_A[-1] == None and ancestors_B[-1] == None:
                common = self.root
            elif ancestors_A[-1] == None:
                common = ancestors_B[-1]
            elif ancestors_B[-1] == None:
                common = ancestors_A[-1]
            else:
                for a_obj in ancestors_A:
                    for b_obj in ancestors_B:
                        if a_obj == b_obj:
                            common = a_obj
                            break
            # common = (set(ancestors_A) & set(ancestors_B)).pop() #Find the common item and remove from set
            # Once the common element is found, calculate the distance
            if flag_a and flag_b:
                dist_a = -1
                dist_b = -1
                degree = abs(abs(dist_a) - abs(dist_b))
                c_type = min(dist_a, dist_b)
            elif flag_a:
                dist_b = -1
                degree = len(ancestors_B)
                c_type = min(dist_a, dist_b)
            elif flag_b:
                dist_a = -1
                degree = len(ancestors_A)
                c_type = min(dist_a, dist_b)
            elif ancestors_A == ancestors_B:
                degree = 0
                c_type = 0
            else:
                dist_a = ancestors_A.index(common)
                dist_b = ancestors_B.index(common)
                degree = abs(abs(dist_a) - abs(dist_b))
                c_type = min(dist_a, dist_b)
            # print 'anc A: ', ancestors_A, ' Common: ', common
            # print 'distances: ',dist_a,dist_b
            # Calculate the final values (degree = abs diff between the two to their ancestors) (c_type = min of
            # distances)

            #print c_type
        return (c_type,degree)


f = Family("a")
f.set_children("a", ["b", "c"])
f.set_children("b", ["d", "e"])
f.set_children("c", ["f", "g"])

f.set_children("d", ["h", "i"])
f.set_children("e", ["j", "k"])
f.set_children("f", ["l", "m"])
f.set_children("g", ["n", "o", "p", "q"])

words = ["zeroth", "first", "second", "third", "fourth", "fifth", "non"]

## These are your test cases.

## The first test case should print out:
## 'b' is a zeroth cousin 0 removed from 'c'

# t, r = f.cousin("b", "c")
# print "'b' is a", words[t],"cousin", r, "removed from 'c'"
# t, r = f.cousin("d", "f")
# print "'d' is a", words[t],"cousin", r, "removed from 'f'"
# t, r = f.cousin("i", "n")
# print "'i' is a", words[t],"cousin", r, "removed from 'n'"
# t, r = f.cousin("q", "e")
# print "'q' is a", words[t], "cousin", r, "removed from 'e'"
# t, r = f.cousin("h", "c")
# print "'h' is a", words[t], "cousin", r, "removed from 'c'"
# t, r = f.cousin("h", "a")
# print "'h' is a", words[t], "cousin", r, "removed from 'a'"
# t, r = f.cousin("h", "h")
# print "'h' is a", words[t], "cousin", r, "removed from 'h'"
# t, r = f.cousin("a", "a")
# print "'a' is a", words[t], "cousin", r, "removed from 'a'"
# t, r = f.cousin("g", "f")
# print "'g' is a", words[t], "cousin", r, "removed from 'f'"
t, r = f.cousin("q", "g")
print "'q' is a", words[t], "cousin", r, "removed from 'g'"
t, r = f.cousin("i", "k")
print "'i' is a", words[t], "cousin", r, "removed from 'k'"

#'b' is a zeroth cousin 0 removed from 'c'
#'d' is a first cousin 0 removed from 'f'
#'i' is a second cousin 0 removed from 'n'
#'q' is a first cousin 1 removed from 'e'
#'h' is a zeroth cousin 2 removed from 'c'
#'h' is a non cousin 3 removed from 'a'
#'h' is a non cousin 0 removed from 'h'
#'a' is a non cousin 0 removed from 'a'

#'g' is a zeroth cousin 0 removed from 'f'
#'q' is a non cousin 1 removed from 'g'
#'i' is a first cousin 0 removed from 'k'