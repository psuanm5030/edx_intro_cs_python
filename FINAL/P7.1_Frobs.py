__author__ = 'Miller'
#!/usr/bin/env python
# -*- coding: utf-8 -*-


class Frob(object):
    def __init__(self, name):
        self.name = name
        self.before = None
        self.after = None
    def setBefore(self, before):
        # example: a.setBefore(b) sets b before a
        self.before = before
    def setAfter(self, after):
        # example: a.setAfter(b) sets b after a
        self.after = after
    def getBefore(self):
        return self.before
    def getAfter(self):
        return self.after
    def myName(self):
        return self.name

def insert(atMe, newFrob):
    """
    atMe: a Frob that is part of a doubly linked list
    newFrob: a Frob with no links
    This procedure appropriately inserts newFrob into the linked list that atMe is a part of.
    """
    # Build the initial list
    # Check if the atMe item is one of the ends
    if atMe.getBefore() == None:
        lst_link = [atMe,atMe.getAfter()]
    elif atMe.getAfter() == None:
        lst_link = [atMe.getBefore(),atMe]
    else:
        lst_link = [atMe.getBefore(),atMe,atMe.getAfter()]
    # Build the list - Front End
    while True:
        first_item = lst_link[0]
        before_item = first_item.getBefore()
        if before_item == None:
            break
        elif before_item not in lst_link:
            lst_link.insert(0,before_item)
        else:
            break
    # Build the list - Back End
    while True:
        last_item = lst_link[-1]
        after_item = last_item.getAfter()
        if after_item == None:
            break
        elif after_item not in lst_link:
            lst_link.append(after_item)
        else:
            break

    # Check the list to determine the position
    position = ''
    insert_lvl = None
    lst_link_str = [l.name for l in lst_link] # List comprehension to figure out where to insert
    if newFrob.name < lst_link_str[0]:
        position = 'first'
    elif newFrob.name > lst_link_str[-1]:
        position = 'last'
    else:
        for n in range(len(lst_link_str)):
            if newFrob.name > lst_link_str[n]:
                ins_lvl = n
    #print 'Trigger: ', position, 'Here is where to insert: ',ins_lvl
    # Once position is gained, then set the links for atMe and newFrob
    if position == 'first':
        newFrob.setAfter(lst_link[0]) # sets FIRST item after newFrob
        lst_link[0].setBefore(newFrob) # sets newFrob before FIRST item
        lst_link.insert(0,newFrob)
    elif position == 'last':
        lst_link[-1].setAfter(newFrob) # sets LAST item after newFrob
        newFrob.setBefore(lst_link[-1]) # sets newFrob before LAST item
        lst_link.append(newFrob)
    else:
        newFrob.setBefore(lst_link[ins_lvl]) # sets XXX before newFrob
        newFrob.setAfter(lst_link[ins_lvl+1]) # sets XXX after newFrob
        lst_link[ins_lvl].setBefore(newFrob)
        lst_link[ins_lvl+1].setAfter(newFrob)
        lst_link.insert(ins_lvl+1,newFrob)
    return



eric = Frob('eric')
andrew = Frob('andrew')
ruth = Frob('ruth')
fred = Frob('fred')
martha = Frob('martha')

andrew.setAfter(eric) # Sets eric after andrew

eric.setBefore(andrew) # Sets andrew before eric
eric.setAfter(fred) # Sets fred after eric

fred.setBefore(eric) # Sets eric before fred
fred.setAfter(martha) # sets martha after fred

martha.setBefore(fred) # sets fred before martha
martha.setAfter(ruth) # sets ruth after martha

ruth.setBefore(martha) # sets martha before ruth

lst = insert(andrew,Frob('g'))
print lst
for l in lst:
    print l.name

print eric.getBefore().name
print eric.getAfter().name

# insert(eric, andrew)
# insert(eric, ruth)
# insert(eric, fred)
# insert(ruth, martha)