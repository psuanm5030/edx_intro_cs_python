__author__ = 'Miller'
#!/usr/bin/env python
# -*- coding: utf-8 -*-


class Frob(object):
    def __init__(self, name):
        self.name = name
        self.before = None
        self.after = None
    def setBefore(self, before):
        # example: a.setBefore(b) sets b before a
        self.before = before
    def setAfter(self, after):
        # example: a.setAfter(b) sets b after a
        self.after = after
    def getBefore(self):
        return self.before
    def getAfter(self):
        return self.after
    def myName(self):
        return self.name

def insert(atMe, newFrob):
    """
    atMe: a Frob that is part of a doubly linked list
    newFrob: a Frob with no links
    This procedure appropriately inserts newFrob into the linked list that atMe is a part of.
    """
    # newFrob Before or After the existing
    position = ''
    if atMe.myName() == newFrob.myName():
        position = 'equal'
    elif atMe.myName() < newFrob.myName():
        position = 'after'
    else:
        position = 'before'
    # Scenerios to consider:
    # (1) You are inserting the newFrob at an end.
    # (2) You are inserting the newFrob immediately next to atMe.
    # (3) You are not inserting the newFrob immediately next to atMe.

    # Scenerio 1: Inserting newFrob at an end.
    if position == 'before' and atMe.getBefore() == None: # If before and there newFrob would be first6
        newFrob.setAfter(atMe) # sets atMe after newFrob
        atMe.setBefore(newFrob) # sets newFrob after atMe
    elif position == 'after' and atMe.getAfter() == None: # If after and there newFrob would be last
        newFrob.setBefore(atMe) # sets atMe before newFrob
        atMe.setAfter(newFrob) # sets newFrob after atMe
    # DONT KNOW WHAT TO DO FOR EQUAL.
    # Scenerio 2: Inserting newFrob immediately next to atMe
    elif position == 'before' and newFrob.myName() > atMe.getBefore().myName():
        newFrob.setAfter(atMe) # sets atMe after newFrob
        atMe.setBefore(newFrob) # sets newFrob before atMe
    elif position == 'after' and newFrob.myName() < atMe.getAfter().myName():
        newFrob.setBefore(atMe) # sets atMe before newFrob
        atMe.setAfter(newFrob) # sets newFrob after atMe
    # Scenerio 3: Inserting newFrob NOT next to atMe

    #Scenerio 1: Immediately next to atMe
    # if position == 'before': # Means newFrob is BEFORE atMe
    #     if atMe.getBefore() != None: # First check if there is anything before atMe - if so, modify
    #         atMe.getBefore().setAfter(newFrob) # Sets newFrob after the previous one
    #     atMe.setBefore(newFrob) # sets newFrob before atMe
    #     newFrob.setAfter(atMe) # sets atMe after newFrob
    # elif position == 'after': # Means newFrob is AFTER atMe
    #     if atMe.getAfter() != None: # First check if there is anything after atMe - if so, modify
    #         atMe.getAfter().setBefore(newFrob) # Sets newFrob before the previous one
    #     atMe.setAfter(newFrob) # sets newFrob after atMe
    #     newFrob.setBefore(atMe) # sets atMe before newFrob

    return

# Test 1:
insert(Frob("gabby"), Frob("allison")) # Adding to the beginning: Walk it forward and backward
print allison.myName()
print allison.getAfter()

#insert(Frob("gabby"), Frob("zara")) # Adding to the end: Walk it forward and backward



# eric = Frob('eric')
# andrew = Frob('andrew')
# ruth = Frob('ruth')
# fred = Frob('fred')
# martha = Frob('martha')
#
# andrew.setAfter(eric) # Sets eric after andrew
#
# eric.setBefore(andrew) # Sets andrew before eric
# eric.setAfter(fred) # Sets fred after eric
#
# fred.setBefore(eric) # Sets eric before fred
# fred.setAfter(martha) # sets martha after fred
#
# martha.setBefore(fred) # sets fred before martha
# martha.setAfter(ruth) # sets ruth after martha
#
# ruth.setBefore(martha) # sets martha before ruth
#
# lst = insert(andrew,Frob('g'))
# print lst
# for l in lst:
#     print l.name
#
# print eric.getBefore().name
# print eric.getAfter().name

# insert(eric, andrew)
# insert(eric, ruth)
# insert(eric, fred)
# insert(ruth, martha)