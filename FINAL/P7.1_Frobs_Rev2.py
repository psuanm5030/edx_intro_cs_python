__author__ = 'Miller'
#!/usr/bin/env python
# -*- coding: utf-8 -*-


class Frob(object):
    def __init__(self, name):
        self.name = name
        self.before = None
        self.after = None
    def setBefore(self, before):
        # example: a.setBefore(b) sets b before a
        self.before = before
    def setAfter(self, after):
        # example: a.setAfter(b) sets b after a
        self.after = after
    def getBefore(self):
        return self.before
    def getAfter(self):
        return self.after
    def myName(self):
        return self.name

def insert(atMe, newFrob):
    """
    atMe: a Frob that is part of a doubly linked list
    newFrob: a Frob with no links
    This procedure appropriately inserts newFrob into the linked list that atMe is a part of.
    """
    # Figure out before or after first
    # Scenerios to consider:
    # (1) You are inserting the newFrob at an end.
    # (2) You are inserting the newFrob immediately next to atMe.
    # (3) You are not inserting the newFrob immediately next to atMe.

    # newFrob comes before atMe:
    if newFrob.myName() < atMe.myName():
        # NOT END: Check to ensure atMe HAS a before node and both atMe and newFrob reference the same before
        if atMe.getBefore() != None and atMe.getBefore() == newFrob.getBefore(): #Meaning they represent same before
            newFrob.setAfter(atMe) # sets atMe after newFrob
            atMe.setBefore(newFrob) # sets newFrob before atMe
            newFrob.before.after = newFrob # IMPORTANT LINE
        # END CASE: If atMe HAS NO before, then set newFrob.
        elif atMe.getBefore() == None: # If this is an end
            atMe.setBefore(newFrob) # sets newFrob before atMe
            newFrob.setAfter(atMe) # sets atMe after newFrob
        # NOT END, but also not representing the same before.
        else:
            newFrob.setAfter(atMe) # Sets atMe after newFrob
            insert(atMe.getBefore(), newFrob) # Recursive function to test the node BEFORE atMe against newFrob
    # newFrob comes after atMe:
    elif newFrob.myName() > atMe.myName():
        # NOT END: Check to ensure atMe HAS an AFTER node and both atMe and newFrob reference the same AFTER
        if atMe.getAfter() != None and atMe.getAfter() == newFrob.getAfter(): #Meaning they represent same after
            newFrob.setBefore(atMe) # sets atMe after newFrob
            atMe.setAfter(newFrob) # sets newFrob before atMe
            newFrob.after.before = newFrob
        # END CASE: If atMe HAS NO after, then set newFrob.
        elif atMe.getAfter() == None: # If this is an end
            atMe.setAfter(newFrob) # sets newFrob before atMe
            newFrob.setBefore(atMe) # sets atMe after newFrob
        # NOT END, but also not representing the same before.
        else:
            newFrob.setBefore(atMe) # Sets atMe before newFrob
            insert(atMe.getAfter(), newFrob) # Recursive function to test the node AFTER atMe against newFrob
    # newFrob has same name as atMe:
    else:
        newFrob.setAfter(atMe.getAfter()) #Set the node after atMe to AFTER newFrob
        atMe.setAfter(newFrob) # Set newFrob after atMe
        newFrob.setBefore(atMe) # Set atMe before newFrob
        if newFrob.getAfter() != None: # If newFrob == None, then set newFrob before node after newFrob
            newFrob.getAfter().setBefore(newFrob)

def findFront(start):
    """
    start: a Frob that is part of a doubly linked list
    returns: the Frob at the beginning of the linked list
    """
    # Base Case
    if start.getBefore() == None:
        return start
    return findFront(start.getBefore())




# Test 1:
# gabby = Frob('gabby')
# allison = Frob('allison')
# insert(gabby, allison) # Adding to the beginning: Walk it forward and backward
# print allison.myName()
# print allison.getAfter().myName()

#insert(Frob("gabby"), Frob("zara")) # Adding to the end: Walk it forward and backward



eric = Frob('eric')
andrew = Frob('andrew')
ruth = Frob('ruth')
fred = Frob('fred')
martha = Frob('martha')

andrew.setAfter(eric) # Sets eric after andrew

eric.setBefore(andrew) # Sets andrew before eric
eric.setAfter(fred) # Sets fred after eric

fred.setBefore(eric) # Sets eric before fred
fred.setAfter(martha) # sets martha after fred

martha.setBefore(fred) # sets fred before martha
martha.setAfter(ruth) # sets ruth after martha

ruth.setBefore(martha) # sets martha before ruth
#
# lst = insert(andrew,Frob('g'))
# print lst
# for l in lst:
#     print l.name
#
# print eric.getBefore().name
# print eric.getAfter().name

# insert(eric, andrew)
# insert(eric, ruth)
# insert(eric, fred)
# insert(ruth, martha)