__author__ = 'Miller'
class Member(object): # Represents a single person in the family
    def __init__(self, founder):
        """
        founder: string
        Initializes a member.
        Name is the string of name of this node,
        parent is None, and no children
        """
        self.name = founder
        self.parent = None
        self.children = []

    def __str__(self):
        return self.name

    def add_parent(self, mother):
        """
        mother: Member
        Sets the parent of this node to the `mother` Member node
        """
        self.parent = mother

    def get_parent(self):
        """
        Returns the parent Member node of this Member
        """
        return self.parent

    def is_parent(self, mother):
        """
        mother: Member
        Returns: Boolean, whether or not `mother` is the
        parent of this Member
        """
        return self.parent == mother

    def add_child(self, child):
        """
        child: Member
        Adds another child Member node to this Member
        """
        self.children.append(child)

    def is_child(self, child):
        """
        child: Member
        Returns: Boolean, whether or not `child` is a
        child of this Member
        """
        return child in self.children


class Family(object): # Represents the whole family tree
    def __init__(self, founder):
        """
        Initialize with string of name of oldest ancestor

        Keyword arguments:
        founder -- string of name of oldest ancestor
        """

        self.names_to_nodes = {}
        self.root = Member(founder) #Creating a member class with the variable passed as the founder
        self.names_to_nodes[founder] = self.root #Create a dictionary with variable (name) and the founder member obj.

    def set_children(self, mother, list_of_children):
        """
        Set all children of the mother.

        Keyword arguments:
        mother -- mother's name as a string
        list_of_children -- children names as strings
        """
        # convert name to Member node (should check for validity)
        mom_node = self.names_to_nodes[mother]
        # add each child
        for c in list_of_children:
            # create Member node for a child
            c_member = Member(c)
            # remember its name to node mapping
            self.names_to_nodes[c] = c_member
            # set child's parent
            c_member.add_parent(mom_node)
            # set the parent's child
            mom_node.add_child(c_member)

    def is_parent(self, mother, kid):
        """
        Returns True or False whether mother is parent of kid.

        Keyword arguments:
        mother -- string of mother's name
        kid -- string of kid's name
        """
        mom_node = self.names_to_nodes[mother]
        child_node = self.names_to_nodes[kid]
        return child_node.is_parent(mom_node)

    def is_child(self, kid, mother):
        """
        Returns True or False whether kid is child of mother.

        Keyword arguments:
        kid -- string of kid's name
        mother -- string of mother's name
        """
        mom_node = self.names_to_nodes[mother]
        child_node = self.names_to_nodes[kid]
        return mom_node.is_child(child_node)

    def getAscendents(self, elt):
        """Assumes receipt of one string, presumably in the family.
        Returns: Listing of all ascendents (ancestors) / nodes."""
        lst, cnt = [], 0
        starter = self.names_to_nodes[elt].get_parent()
        if starter == None:
            #print 'No Ascendents of ', elt
            return lst
        else:
            lst.append(self.names_to_nodes[elt].get_parent())
            while True:
                parent = lst[-1].get_parent()
                if parent == None:
                    break
                else:
                    lst.append(parent)
        return lst

    def nodeDescendent(self, lst1, lst2):
        """Assumes receipt of two lists of ascendents.
        Returns: True of False if the nodes are descendents of each other."""
        if lst1 == lst2:
            return True
        return False

    def nodeCommon(self,lst1,lst2):
        """Assumes receipt of two lists of nodes.
        Returns: Common node between two members as an object."""
        #Returns the LAST common node between the two, so the closest node to each.
        return list(set(lst1) & set(lst2))[0]

    def nodeDistance(self,common,lst1,lst2):
        """Assumes receipt of the node listings for each for distance calculation.
        Returns: Tuple of the distances between the common node and the elements."""
        if len(lst1) == 0 and len(lst2) ==0 :
            return (0,0)
        elif len(lst1) == 0:
            return (0,lst2.index(common))
        elif len(lst2) == 0:
            return (lst1.index(common),0)
        else:
            dist1 = lst1.index(common)
            dist2 = lst2.index(common)
            return (dist1,dist2)

    def cousin(self, a, b):
        """
        Returns a tuple of (the cousin type, degree removed)

        Keyword arguments:
        a -- string that is the name of node a
        b -- string that is the name of node b

        cousin type:
          -1 if a and b are the same node.
          -1 if either one is a direct descendant of the other
          >=0 otherwise, it calculates the distance from
          each node to the common ancestor.  Then cousin type is
          set to the smaller of the two distances, as described
          in the exercises above

        degrees removed:
          >= 0
          The absolute value of the difference between the
          distance from each node to their common ancestor.
        """
        # Start off by getting lists of ascendents for both nodes.
        ancestors1 = self.getAscendents(a)
        ancestors2 = self.getAscendents(b)
        # Check if they are the same node, if so, return -1 (non-cousin) and 0 (degrees removed).
        if self.names_to_nodes[a] == self.names_to_nodes[b]:
            return (-1,0)
        # Check if they are direct descendents (parent / child), if so, return -1 (non-cousin) and 1 (degrees removed).
        elif self.is_parent(a,b) or self.is_child(a,b):
            return (-1,1)
        # Check if any of the listings returned blank (no ancestors).  If so, set common ancestor to that node (
        # founder) and calculate the distance.  Return -1 (non-cousin) and the calculated distance (degrees removed).
        elif len(ancestors1) == 0:
            common = self.names_to_nodes[a]
            dist1, dist2 = self.nodeDistance(common,ancestors1,ancestors2)
            degree = abs(dist1-dist2)+1
            #c_type = min(dist1,dist2)
            return (-1,degree)
        # Same as above but in the reverse.
        elif len(ancestors2) == 0:
            common = self.names_to_nodes[b]
            dist1, dist2 = self.nodeDistance(common,ancestors1,ancestors2)
            degree = abs(dist1-dist2)+1
            #c_type = min(dist1,dist2)
            return (-1,degree)
        # If the have the SAME ascendents, then they must be on the same level, meaning "Zeroeth" Cousins. Calculate
        # the distance from the common.
        elif self.nodeDescendent(ancestors1,ancestors2):
            common = self.nodeCommon(ancestors1,ancestors2)
            dist1,dist2 = self.nodeDistance(common,ancestors1,ancestors2)
            degree = abs(dist1-dist2)
            return (0,degree)
        # Finally, check the rest, particularly calculating the distance to the common ancestor and the degrees removed.
        else:
            common = self.nodeCommon(ancestors1,ancestors2)
            dist1,dist2 = self.nodeDistance(common,ancestors1,ancestors2)
            degree = abs(dist1-dist2)
            c_type = min(dist1,dist2)
            return (c_type,degree)


f = Family("a")
f.set_children("a", ["b", "c"])
f.set_children("b", ["d", "e"])
f.set_children("c", ["f", "g"])

f.set_children("d", ["h", "i"])
f.set_children("e", ["j", "k"])
f.set_children("f", ["l", "m"])
f.set_children("g", ["n", "o", "p", "q"])

words = ["zeroth", "first", "second", "third", "fourth", "fifth", "non"]

## These are your test cases.

## The first test case should print out:
## 'b' is a zeroth cousin 0 removed from 'c'

t, r = f.cousin("b", "c")
print "'b' is a", words[t],"cousin", r, "removed from 'c'"
t, r = f.cousin("d", "f")
print "'d' is a", words[t],"cousin", r, "removed from 'f'"
t, r = f.cousin("i", "n")
print "'i' is a", words[t],"cousin", r, "removed from 'n'"
t, r = f.cousin("q", "e")
print "'q' is a", words[t], "cousin", r, "removed from 'e'"
t, r = f.cousin("h", "c")
print "'h' is a", words[t], "cousin", r, "removed from 'c'"
t, r = f.cousin("h", "a")
print "'h' is a", words[t], "cousin", r, "removed from 'a'"
t, r = f.cousin("h", "h")
print "'h' is a", words[t], "cousin", r, "removed from 'h'"
t, r = f.cousin("a", "a")
print "'a' is a", words[t], "cousin", r, "removed from 'a'"
t, r = f.cousin("g", "f")
print "'g' is a", words[t], "cousin", r, "removed from 'f'"
t, r = f.cousin("q", "g")
print "'q' is a", words[t], "cousin", r, "removed from 'g'"
t, r = f.cousin("i", "k")
print "'i' is a", words[t], "cousin", r, "removed from 'k'"

#'b' is a zeroth cousin 0 removed from 'c'
#'d' is a first cousin 0 removed from 'f'
#'i' is a second cousin 0 removed from 'n'
#'q' is a first cousin 1 removed from 'e'
#'h' is a zeroth cousin 2 removed from 'c'
#'h' is a non cousin 3 removed from 'a'
#'h' is a non cousin 0 removed from 'h'
#'a' is a non cousin 0 removed from 'a'
#'g' is a zeroth cousin 0 removed from 'f'
#'q' is a non cousin 1 removed from 'g'
#'i' is a first cousin 0 removed from 'k'