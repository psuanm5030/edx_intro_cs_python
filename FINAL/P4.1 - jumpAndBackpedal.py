__author__ = 'Miller'
def jumpAndBackpedal(isMyNumber):
    '''
    isMyNumber: Procedure that hides a secret number.
     It takes as a parameter one number and returns:
     *  -1 if the number is less than the secret number
     *  0 if the number is equal to the secret number
     *  1 if the number is greater than the secret number

    returns: integer, the secret number
    '''
    guess = 1
    if isMyNumber(guess) == 0:  # Was 1 - should be 0
        return guess
    foundNumber = False
    while not foundNumber:
        sign = isMyNumber(guess)
        if sign == -1:
            guess += 1  # Was *= 2 should be += 1
            print 'less than'
        elif sign == 1:
            guess -= 1
            print 'greater than'
        elif sign == 0:
            foundNumber = True
            print 'found it!',guess
    return guess

def isMyNumber(guess):
    secret = 1
    if guess < secret:
        return -1
    elif guess > secret:
        return 1
    elif guess == secret:
        return 0

print jumpAndBackpedal(isMyNumber)

def testing():
    trig = False
    lst = [1,2,3,4,5,6,7]
    cnt = 0
    while not trig:
        if lst[cnt] < 5:
            print lst[cnt]
            cnt += 1
        else:
            trig = True

    return 'done at: ', lst[cnt]

#print testing()