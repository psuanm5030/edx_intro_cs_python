#Lecture 6 - Tuples 
#build a program that accepts a Tuple and returns a tuple of the odd components (in a tuple)

def oddTuples(aTup):
    '''
    aTup: a tuple
    
    returns: tuple, every other element of aTup. 
    '''
    cnt = 0
    nTup = ()
    for a in aTup:
        cnt+=1
        if cnt%2 != 0:
            nTup += (a,)
    return nTup
    

a = ('I', 'am', 'a', 'test', 'tuple')
print oddTuples(a)