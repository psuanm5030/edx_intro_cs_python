def gcdIter(a, b):
    '''
    a, b: positive integers
    
    returns: a positive integer, the greatest common divisor of a & b.
    '''
    iter = min(a,b)
    lcdiv = 1
    for i in range(2,(iter+1)):
        if a%i == 0 and b%i == 0:
            lcdiv = i
    return lcdiv
    
print gcdIter(17,12)