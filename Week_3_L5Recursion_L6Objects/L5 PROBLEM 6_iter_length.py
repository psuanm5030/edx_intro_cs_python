#L5P6 - Length of a string
#Write an iterative function, lenIter, which computes the length of an input argument (a string), 
#by counting up the number of characters in the string.

def lenIter(aStr):
    '''
    aStr: a string
    
    returns: int, the length of aStr
    '''
    result = 0
    for c in aStr: 
        result += 1
    return result