#lection 5: Recursive
#Fibonacci numbers

def fib(x):
    """assumes x an int >= 0
       returns Fibonacci of x"""
    #ASSERT - if both of these are true, THEN the body will be evaluated
    assert type(x)==int and x>=0 
    if x==0 or x==1:
        return 1
    else:
        return fib(x-1)+fib(x-2)
    
    