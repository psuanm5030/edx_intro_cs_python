#Lecture 5: Recursive
#Towers of Hanoi
#solve a smaller problem
#solve the basic problem
#solve a smaller problem

def printMove(fr, to):
    print('move from '+str(fr)+' to '+str(to))
    
def Towers(n, fr, to, spare):
    if n == 1:
        printMove(fr, to)
    else:
        Towers(n-1, fr, spare, to)
        Towers(1, fr, to, spare)
        Towers(n-1, spare, to, fr)
        
Towers(3,'from','target','spare')