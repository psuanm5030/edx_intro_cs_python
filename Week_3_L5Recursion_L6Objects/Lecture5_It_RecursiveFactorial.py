# Lecture 5: Recursive Algorithm
# Factorial - iterative AND Recursive

def itFact(n):
    result=1
    while n > 1: 
        result = result * n  #"Update Variables"
        n -= 1
    return result

def recFact(n):
    if n == 1:
        return n
    return (n * recFact(n-1))

print itFact(25)
print recFact(25)