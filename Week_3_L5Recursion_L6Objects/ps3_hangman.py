# 6.00 Problem Set 3
# 
# Hangman game
#

# -----------------------------------
# Helper code
# You don't need to understand this helper code,
# but you will have to know how to use the functions
# (so be sure to read the docstrings!)

import random
import string

WORDLIST_FILENAME = "words.txt"

def loadWords():
    """
    Returns a list of valid words. Words are strings of lowercase letters.
    
    Depending on the size of the word list, this function may
    take a while to finish.
    """
    print "Loading word list from file..."
    # inFile: file
    inFile = open(WORDLIST_FILENAME, 'r', 0)
    # line: string
    line = inFile.readline()
    # wordlist: list of strings
    wordlist = string.split(line)
    print "  ", len(wordlist), "words loaded."
    return wordlist

def chooseWord(wordlist):
    """
    wordlist (list): list of words (strings)

    Returns a word from wordlist at random
    """
    return random.choice(wordlist)

# end of helper code
# -----------------------------------

# Load the list of words into the variable wordlist
# so that it can be accessed from anywhere in the program
wordlist = loadWords()

def isWordGuessed(secretWord, lettersGuessed):
    '''
    secretWord: string, the word the user is guessing
    lettersGuessed: list, what letters have been guessed so far
    returns: boolean, True if all the letters of secretWord are in lettersGuessed;
      False otherwise
    '''
    guessed = ''
    sw1 = secretWord
    for letter in lettersGuessed:
        guessed += letter
    sw1 = sw1.translate(None,guessed)
    if sw1 == '':
        return True
    else:
        return False

#secretWord = 'apple'
#lettersGuessed = ['e', 'i', 'k', 'p', 'r', 's']
#print isWordGuessed(secretWord, lettersGuessed)

def getGuessedWord(secretWord, lettersGuessed):
    '''
    secretWord: string, the word the user is guessing
    lettersGuessed: list, what letters have been guessed so far
    returns: string, comprised of letters and underscores that represents
      what letters in secretWord have been guessed so far.
    '''
    guessed = ''
    not_guessed = string.ascii_lowercase # simply returns --> 'abcdefghijklmnopqrstuvwxyz'
    sw2 = secretWord
    for letter in lettersGuessed:
        guessed += letter
        if letter in not_guessed:
            not_guessed = not_guessed.translate(None,letter)
    #print 'guessed: ',guessed,' Not Guessed: ',not_guessed
    for letter in secretWord:
        if letter in not_guessed:
            sw2 = sw2.replace(letter, '_ ') # replaces the letter with an underscore
    return sw2


#secretWord = 'apple'
#lettersGuessed = ['e', 'i', 'k', 'p', 'r', 's']
#print getGuessedWord(secretWord, lettersGuessed)

def getAvailableLetters(lettersGuessed):
    '''
    lettersGuessed: list, what letters have been guessed so far
    returns: string, comprised of letters that represents what letters have not
      yet been guessed.
    '''
    guessed = ''
    not_guessed = string.ascii_lowercase # simply returns --> 'abcdefghijklmnopqrstuvwxyz'
    for letter in lettersGuessed:
        guessed += letter
        if letter in not_guessed:
            not_guessed = not_guessed.translate(None,letter)
    return not_guessed
    
#lettersGuessed = ['e', 'i', 'k', 'p', 'r', 's']
#print getAvailableLetters(lettersGuessed)

def hangman(secretWord):
    '''
    secretWord: string, the secret word to guess.

    Starts up an interactive game of Hangman.

    * At the start of the game, let the user know how many 
      letters the secretWord contains.

    * Ask the user to supply one guess (i.e. letter) per round.

    * The user should receive feedback immediately after each guess 
      about whether their guess appears in the computers word.

    * After each round, you should also display to the user the 
      partially guessed word so far, as well as letters that the 
      user has not yet guessed.

    Follows the other limitations detailed in the problem write-up.
    '''
    print 'Welcome to the game, Hangman!'
    word_len = len(secretWord)
    guesses = 8
    lettersGuessed = []
    cnt = guesses
    print 'I am thinking of a word that is',word_len,'letters long.'
    #for guess in range(guesses):
    while guesses >0:
        trigger = True
        while trigger == True:
            print '--------------'
            print 'You have',guesses,'guesses left.'
            print 'Available letters: ',getAvailableLetters(lettersGuessed)
            guessedLetter = raw_input('Please guess a letter: ').lower()
            if guessedLetter in secretWord and guessedLetter in getAvailableLetters(lettersGuessed):
                trigger = False
                lettersGuessed.append(guessedLetter)
                print 'Good Guess: ',getGuessedWord(secretWord,lettersGuessed)
                #cnt -= 1
            elif guessedLetter not in getAvailableLetters(lettersGuessed):
                print 'Oops! You\'ve already guessed that letter: ',getGuessedWord(secretWord,lettersGuessed)
            elif guessedLetter not in secretWord and guessedLetter in getAvailableLetters(lettersGuessed):
                trigger = False
                lettersGuessed.append(guessedLetter)
                print 'Oops! That letter is not in my word: ',getGuessedWord(secretWord,lettersGuessed)
                #cnt -= 1
                guesses -= 1
        if isWordGuessed(secretWord,lettersGuessed) == True:
            print '--------------'
            return 'Congratulations, you won!'
        #elif isWordGuessed(secretWord,lettersGuessed) == False and cnt ==0:
        elif isWordGuessed(secretWord,lettersGuessed) == False and guesses ==0:
            print '--------------'
            return 'Sorry, you ran out of guesses. The word was %s.' % secretWord
        #if getGuessedWord(secretWord,lettersGuessed) == secretWord:
        #    print 'Congratulations, you won!'
        #    break
        #elif getGuessedWord(secretWord,lettersGuessed) != secretWord and cnt == 0:
        #    print 'Sorry, you ran out of guesses. The word was',secretWord,'.'
        #    break

# When you've completed your hangman function, uncomment these two lines
# and run this file to test! (hint: you might want to pick your own
# secretWord while you're testing)

#secretWord = chooseWord(wordlist).lower()
secretWord = 'chode'
print hangman(secretWord)
