#Lecture 6 - Lists Practice - the idea of Cloning

#first function is bad as the list that is being iterated
#over is being mutated
def removeDups(l1,l2):
    for e1 in l1:
        if e1 in l2:
            l1.remove(e1)

#this function is much better as it CLONES the list to 
#make an iterator list    
def removeDupsBetter(l1,l2):
    l1start = l1[:] #CANNOT DO l1start = l1 --> need the brackets - OTHERWISE, its simply assignment
    for e1 in l1start:
        if e1 in l2: 
            l1.remove(e1)