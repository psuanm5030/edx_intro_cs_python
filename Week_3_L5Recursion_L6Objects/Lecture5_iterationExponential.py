# Lecture 5: Iterative Algorithm
# Exponential

def iterPower(base, exp):
    '''
    base: int or float.
    exp: int >= 0
 
    returns: int or float, base^exp
    '''
    if exp == 0: 
        result = 1.00
        return result
    result = base
    while exp > 1:
        result *= base
        exp -= 1
    return result

print iterPower(2,0)