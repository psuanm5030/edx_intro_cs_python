# Lecture 5: Iterative Algorithm
# Multiplication

def iterMul(a,b):
    result = 0 
    while b > 0:
        result += a
        b-=1
    return result

print iterMul(334,5238)