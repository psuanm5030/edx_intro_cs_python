#L5P8 - Bisection Search - recursively
#Implement the function isIn(char, aStr) which implements the above idea recursively 
#to test if char is in aStr. char will be a single character and aStr will be a string 
#that is in alphabetical order. The function should return a boolean value.

def lenRecur(aStr):
    '''
    aStr: a string
    
    returns: int, the length of aStr
    '''
    if aStr == '':
        return 0
    return (1+ lenRecur(aStr[:-1]))