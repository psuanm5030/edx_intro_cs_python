#Lecture 6 - Dictionaries
# This time, write a procedure, called biggest, which returns the key corresponding to the entry with the
# largest number of values associated with it. If there is more than one such entry,
# return any one of the matching keys.

animals = {'a': [2, 3], 'c': [5, 19, 6, 7, 6, 14, 19, 3, 8], 'b': [13, 5, 8, 15, 6]}
#correct output is 'c'

biggest = None
biggest_cnt = 0
for k,v in animals.iteritems():
    cnt = 0
    for e in v:
        cnt += 1
        print cnt
    print 'Key: ',k,' Number: ',cnt
    if biggest == None:
        biggest = k
        biggest_cnt = cnt
    elif cnt > biggest_cnt:
        biggest = k
        biggest_cnt = cnt
    #print 'Biggest is now: ',biggest
print biggest