# Lecture 5: Recursive Algorithm
# Multiplication

def recurMul(a, b):
    '''
    base: int or float.
    exp: int >= 0
 
    returns: int or float, base^exp
    '''
    if b ==1:    #BASE CASE
        return a 
    else:       #Otherwise, perform recursive step!
        return a + recPower(a, b-1)
        
print recurMul(5,5)

#Base case, we can show that recurMul must return the correct answer
#For recursive case (else), we can assume that recurMul correctly returns 
#an answer for problems of size smaller than b, then by the addition step,
# it must also return a correct answer for problem of size b.