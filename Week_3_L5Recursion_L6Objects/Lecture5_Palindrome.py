# Lecture 5: Recursive
# Palindrome
# example of a divide-and-conquer problem - break into smaller problems, then
# the solutions of the sub-problems can be combined to solve the original
# in this case: Checking two things: 1) do the first and last chars match?, and
# 2)A simpler version of the same problem --> check remaining middle portion to see
# if its a palindrome.

def isPalindrome(s):
    def toChars(s):
        s = s.lower()
        ans = ''
        for c in s:
            if c in 'abcdefghijklmnopqrstuvwxyz':
                ans += c
        return ans

    def isPal(s):
        if len(s) <= 1:
            return True
        else:
            # checking if first and last chars are the same, then
            # does this again
            return s[0] == s[-1] and isPal(s[1:-1])

    # THIS IS KEY: this calls all the internal functions
    return isPal(toChars(s))


print isPalindrome('able was I ere I saw elba')