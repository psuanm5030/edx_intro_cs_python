#Lecture 6 - Dictionaries

animals = { 'a': ['aardvark'], 'b': ['baboon'], 'c': ['coati']}
animals['d'] = ['donkey']
animals['d'].append('dog')
animals['d'].append('dingo')

cnt = 0
for k,v in animals.iteritems():
    for e in v:
        cnt += 1
    print cnt