#L5P - Length of a string
#For this problem, write a recursive function, lenRecur, which computes the length of an input argument (a string), 
#by counting up the number of characters in the string.

def isIn(char, aStr):
    '''
    char: a single character
    aStr: an alphabetized string
    
    returns: True if char is in aStr; False otherwise
    '''
    print 'instance - middle',(len(aStr)/2),' char: ',aStr[len(aStr)/2]
    if aStr == '':
        return False
    elif len(aStr) == 1:
        return char == aStr
    elif aStr[(len(aStr)/2)] == char:
        return True
    elif aStr[(len(aStr)/2)] > char: #greater than test char?  Check the lower half
        return isIn(char,aStr[:(len(aStr)/2)])
    elif aStr[(len(aStr)/2)] < char: #less than test char?  Check the upper half
        return isIn(char,aStr[(len(aStr)/2):])
    
print isIn('r','ccghjlmmrtvwxy')