# Problem Set 3 - Problem 1
# Radiation Exposure Measurement
# Test Case #1:
# radiationExposure(0, 5, 1)
# answer = 39.10318784326239

def f(x):
    import math
    return 10*math.e**(math.log(0.5)/5.27 * x)

def radiationExposure(start, stop, step):
    '''
    Computes and returns the amount of radiation exposed
    to between the start and stop times. Calls the 
    function f (defined for you in the grading script)
    to obtain the value of the function at any point.
 
    start: integer, the time at which exposure begins
    stop: integer, the time at which exposure ends
    step: float, the width of each rectangle. You can assume that
      the step size will always partition the space evenly.

    returns: float, the amount of radiation exposed to 
      between start and stop times.
    '''
    lst = [start,]
    start1 = start
    stop1 = stop
    while start1 < stop1:
        lst.append(start1+step)
        start1 += step
    #print lst
    total = 0
    for i in lst[:-1]:
        total = total + (step*f(i))
        #print total
    #for i in range(start, stop, step):
    #    total = total + (step*f(i))
    return total

print radiationExposure(0, 5, 1) # answer should be 39.10318784326239
print radiationExposure(5, 11, 1) # answer should be 22.94241041057671
print radiationExposure(0, 4, 0.25) # answer should be XX
print radiationExposure(40, 100, 1.5) #answer should be 0.434612356115