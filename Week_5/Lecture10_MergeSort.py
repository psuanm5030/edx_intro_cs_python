__author__ = 'Miller'

import operator

def merge(left,right,compare):
    result = []
    i,j = 0,0
    while i < len(left) and j < len(right):
        if compare(left[i],right[j]):
            result.append(left[i])
            i += 1
        else:
            result.append(right[j])
            j += 1
    while (i < len(left)):
        result.append(left[i])
        i += 1
    while (j < len(right)):
        result.append(right[j])
        j+= 1
    return result

def mergeSort(L,compare = operator.lt):
    if len(L) <2:
        print 'return: ',L[:]
        return L[:] # if list is of length 0 or 1, then im done --> return the list
    else:
        middle = int(len(L)/2)
        print 'middle: ',middle
        left = mergeSort(L[:middle],compare) # breaking it down until just merging lists length are 0 or 1
        print 'left: ', left
        right = mergeSort(L[middle:],compare)
        print 'right: ', right
        print 'merge!!: ', merge(left, right, compare)
        return merge(left, right, compare)
# Complexity of this is Log Linear - which is much better than selection or bubble sorts which are quadratic
# it does come at the cost of space, as it makes a new list
anm = [5,3,2,4]
print mergeSort(anm)