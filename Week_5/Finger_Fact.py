__author__ = 'Miller'
# want to measure the complexity or number of steps

def fact(n):
    answer = 1 # ONE step
    while n > 1: # loop is FIVE steps
        answer *= n # this is TWO steps - one for multiplication, one for assignment
        n -= 1 # this is TWO steps - one for multiplication, one for assignment
    return answer # ONE step
# 5*n + 2 steps

print fact(5)